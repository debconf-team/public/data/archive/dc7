#!/usr/bin/python
import cgi
import os
import sys
sys.path.append("/var/www/dc7_debday/other")
import logger
import time
import configparser
import cgitb
cgitb.enable()

from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
import email.Message
from string import letters
import random

def mail(to, frm, subject, text, sendmail):
    msg = email.Message.Message()
    msg['From'] = frm
    msg['To'] = COMMASPACE.join(to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject
    msg.set_payload(text)
    p=os.popen("%s -t" % sendmail, 'w')
    p.write(msg.as_string())
    exitcode = p.close()
    return exitcode
    
def ipcheck(ip, db):
    
    d=db.execute("SELECT ip, count FROM ip WHERE ip = \'%s\'" % ip)
    data = d.fetchone()
    if(data==None):
        db.execute("INSERT INTO ip (ip, count) VALUES (\'%s\', 0)" % ip)
        return False
    else:
        count=data[1]
        db.execute("UPDATE ip SET count = %d WHERE ip = \'%s\'" % (count+1, ip))
        return (count>int(config['ipcount']))

def randstr():
    return "".join(random.sample(letters, 10))
def inlist(email, db):
    d=db.execute("SELECT * FROM list where address = \'%s\'" % email)
    return (d.fetchall()!=[])
def inpending(email, db):
    d=db.execute("SELECT * FROM pending where address = \'%s\'" % email)
    return (d.fetchall()!=[])
print "Content-Type: text/html\n\n"

config=configparser.configparser("../other/debian_day.config")
db = logger.logger(config['logpath'], config['server'], config['db'], config['username'], config['password'])

ip=(os.environ["REMOTE_ADDR"])
form=cgi.FieldStorage()
if ipcheck(ip, db):
    print config['iperror']
elif( not form.has_key("email") or (form['email'].value)==''): 
    print config['parseerror']
elif(inlist((form['email']).value, db)):
     print config['inlisterror']
elif(inpending((form['email']).value, db)): print config['inpendingerror']
else:
    address = (form['email']).value
    rand = randstr()
    db.execute("INSERT INTO pending (address, ip, rand) VALUES (\'%s\', \'%s\', \'%s\')" % (address, ip, rand))
    print config['signupsucess']
    body = config['body'] % (config['baseurl'], rand)
    mail([config['to'], address], config['from'], config['subject'], body, config['sendmail'])
db.close()


    
	
