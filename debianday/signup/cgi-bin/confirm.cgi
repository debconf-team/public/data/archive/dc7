#!/usr/bin/python
import cgi
import os
import sys
sys.path.append("/var/www/dc7_debday/other")
import logger
import time
import configparser
import cgitb
cgitb.enable()

def ipcheck(ip, db):
    
    d=db.execute("SELECT ip, count FROM ip WHERE ip = \'%s\'" % ip)
    data = d.fetchone()
    if(data==None):
        db.execute("INSERT INTO ip (ip, count) VALUES (\'%s\', 0)" % ip)
        return False
    else:
        count=data[1]
        db.execute("UPDATE ip SET count = %d WHERE ip = \'%s\'" % (count+1, ip))
        return (count>int(config['ipcount']))

print "Content-Type: text/html\n\n"

config=configparser.configparser("../other/debian_day.config")
db = logger.logger(config['logpath'], config['server'], config['db'], config['username'], config['password'])

ip=(os.environ["REMOTE_ADDR"])
form=cgi.FieldStorage()
if ipcheck(ip, db):
    print config['iperror']
elif( not form.has_key("str") or (form['str'].value)==''): 
    print config['parseerror']
else:
    rand = (form['str']).value
    l = db.execute("SELECT address FROM pending WHERE rand = \'%s\'" % rand )
    data=l.fetchone()
    if(data==None):
        print config['parseerror']
    else:
        print config['confirmsucess']
        email = data[0]
        l= db.execute("SELECT address FROM list WHERE address = \'%s\'" % email)
        data=l.fetchone()
        if(data==None):
            db.execute("INSERT INTO list (address, ip) VALUES (\'%s\', \'%s\')" % (email, ip))
            db.execute("DELETE FROM pending WHERE rand = \'%s\'" % rand)
db.close()


    
	
