from time import gmtime, strftime, time
from os import getpid
import pgdb
class logger(object):
    f=None
    key=None
    def __init__(self, filename, server, db, username, password):
	self.connection=pgdb.connect(':'.join([server, db, username, password]))
	key=str(getpid()+int(time()))
	monthstr=strftime("%B", gmtime())
	self.f=open(filename+'/'+monthstr, "a+", 1)
	self.log("connected to dbase")
    def log(self, str):
	timestamp=strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())
	self.f.write((timestamp + ": "+ str + "\n"))
	self.f.flush()
    def execute(self, sql):
	self.log("running query: " + sql)
        c=self.connection.cursor()
	c.execute(sql)
        self.connection.commit()
	return c
    def close(self):
	self.f.close()



