def concat(lst):
    f=""
    for a in lst: f+=a
    return f
def quoteparse(value):
    esc = False
    output=''
    done = False
    for char in value:
        if((char=='\\') and (not esc)):
            esc = True
        elif((char=="\\") and (esc)):
            output+='\\'
            esc = False
        elif((char=='\"') and (not esc)):
            done = True
            break
        elif((char=='\"') and (esc)):
            output+='\"'
        else:
            output+=char
    return (output, done)

class configparser(dict):
    def __init__(self, file):
         super(configparser, self).__init__()
         f=open(file, "r")
         line=f.readline()
         while line!='':
             if(line[0]!='#'):
                 bits = line.split("=")
                 key=bits[0].strip()
                 value = concat(bits[1:])
                 value=value.strip()
                 if((value != '') and ("\"" in value[0])):
                     value=value[1:]
                     (output, done) = quoteparse(value)
                     while not done:
                         o, done = quoteparse(f.readline())
                         output+=o
                     self[key]=output
                 elif ((key!='') and (value!='')):
                     self[key]=value
             else:
                 pass
             line=f.readline()
              
#import sys
#c = configparser(sys.argv[1])
#print c
