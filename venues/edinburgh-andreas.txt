Impressions
===========
tall, old houses with stately, manorial look
traffic on the other side of the road.
lots of cobbled streets
rain? - rain!

goals
=====

raise awareness about free software
goverment might become interested in the free software part

running the conference

local "debian feeling" can be improve, debian users can get drawn
further into the community

local team
==========

perhaps many lug members and users are interested in helping(we
met not many, personally)

central figures with the ability to coordinate (and take over the
local lead, if necessary)
murray
neil
kevin



Physicians
==========
could not get in - film at 11?

problems with accessibility: cobbled street at the
weelchair entrance

generic remarks: 
- most expensive option
- seems to lack a natural place to meet or hang out in


Tiviot
======

oldish place, a bit dark, used, partly worn, in
other parts undergoing renovation, or intended renovation.

flexible about cables and own installations

many differently sized rooms:
- two big ones (200-250ppl)	
- five (?) medium ones

cantina (100ppl, £6/meal (==8.29€)) available

pub, run by the house (== not as inexpensive as we would wish)

garden across the streat, which is no central or natural place to
meet to hang out

seem to be reluctant to open 24h, which needs checking.

hostels (£10-£11 or 14€-15€) are 7-10min walking distance
up/downhill along busy public streets


Arts College
============

middel old, used and parly worn (due to lots of exhibitions?) but
less so then Tiviot, and light place.
quite dusty. 

we took an inofficial tour of the place, no chance to quiz the
people in charge.

public, but disclosed garden inside (hang-out place?)

perhaps flexible about things like cables and opening hours
(needs checking!)

two hotels close by: £77 (106€) and £135(186€)

hostels (£10-£11 or 14€-15€) are 7-10min walking distance
up/downhill along busy public streets

restaurant(100-200ppl) in the same complex.

Costs of restaurant and other small rooms? Cleaning costs for
garden?

Summary
=======

All these place do not have accommodation for the majority of
people close by. That means that, in order to get the "debconf
feeling", the social center of gravity would need to be moved
from the accommodation (where it was in past years) to the
talks/hack places. (This would be an experiement we have not
attempted before, and while it is possible to succeed it would
put the social ascpect of debconf in danger.)

Debconf would then work like this: We would not provide network
at the hostels so that people are encouraged to come to the
conference venue all day. Food, talks, hacking and socializing
would take place there. At the end of their individual
work/party/conference day people would spread out over the
surrounding hotels and hostels to their sleeping quarters. The
distance to the hostels is most likely too long for footsore
people to return to the conference/sleeping quarters several
times per day.

So the Edinburgh Debconf venues must (imho) additionally provide 
- opening hours from 00:00-23:59
- a social focal point for party and hang-out
in order to compensate.

The only place that could offer a natural hang-out place seems to
be the Arts College, which has a disclosed garden. According to
our experience this would be the place where the desired "debconf
feeling" could come to be. I do not regard the other places as 

The accommodation, food and venue costs for the Art College
and the hostels would be at least 

             ppl     days    cost
Food:      (300+50) * 7*3  * 3.45 
hostels:  +(300+50) * 7    * 14
venue:
 Debconf  +           7    * (218+725+509) 
 Debcamp  +           7    * (218)

cut and paste in bc:
overall:
(300+50)* 7 * (3 * 3.45 + 14) + ( 218+725+509 + 218)*7
71347.50

per day/per person
( (300+50)* 7 * (3 * 3.45 + 14) + ( 218+725+509 + 218)*7 ) / (300 +50) / 7
29

this would change with more probable food prices:

( (300+70)* 7 * (3 * 6.2 + 14) + ( 218+725+509 + 218)*7 ) / (300
+70) / 7
37
( (300+70)* 7 * (3 * 6.2 + 14) + ( 218+725+509 + 218)*7 )
96124.0

