\section{Introduction}

\subsection{Embedded Development the Debian way.}

Embedded Debian is an official sub-project of Debian aimed at making Debian GNU
/Linux a mainstream choice for embedded projects.

Debian's multi-architecture support, vendor independence, social contract and
huge software base make it an attractive choice for all sorts of systems, but
the main distribution is very much aimed at systems with at least desktop
resources (big hard discs, plenty of memory). Embedded Debian tries to strip
Debian down to be a much smaller system whilst still keeping all the good things.

It is also an open development environment to cover the multitude of possible
ways of building small GNU/Linux systems. Different methodologies are
appropriate, depending on the resources and functionality of the target system
and the development environment. Embedded Debian exists to encourage work on
all possible methods that build from Debian's coherent source tree or use its
multi-architecture build tools. The common thread is free software and open
development. The various development threads are organised as sub-projects.

There are a multitude of efforts currently underway to bring Linux to the
embedded systems space. These are being worked on by groups ranging from
small non-commercial development communities up to large commercial
organisations. Because of the open source nature of Linux and the
distributions built on Linux, there is quite a bit of cross-pollination
between these groups. However, there doesn't seem to be any one open place
providing a focus for these efforts.

This leads to a situation in which there are several distributions that are
good, but not as good as they could be, because they suffer from one or more of
the following:

\subsection{Problems of embedded platforms outside Debian}

\begin{itemize}
  \item{Support only one or two platforms}
  \item{Are based on older technology}
  \item{Are difficult to keep current}
  \item{Do not have a large development community}
  \item{Are developed in a closed community}
  \item{Contain non-free components}
\end{itemize}

The solution is to concentrate all that is good in the embedded Linux world.
Linux and Debian GNU/Linux have gained momentum partly because of the critical
masses they have achieved. We believe that embedded Linux can gain far more
momentum than it already has if a critical mass of development activity,
focused in the right place, can be achieved.

We believe Debian is that place, and that a concentrated effort has the
potential to advance the state of the art of embedded Linux faster and more
effectively then do the fragmented efforts that exist today.

Whereas other Linux distributions are developed by individuals, small closed
groups or commercial vendors, Debian is the only Linux distribution that is
being developed cooperatively by many individuals through the Internet, in the
same spirit as Linux and other free software.

Although Debian GNU/Linux itself is free software, it is a base upon which
value-added Linux distributions can be built. By providing a reliable,
full-featured base system, Debian provides Linux users with increased
compatibility, and allows Linux distribution creators to eliminate duplication
of effort and focus on the things that make their distribution special.

We believe that Embedded Debian can provide a vendor neutral reference
distribution for the benefit of individual developers, organisations who want
to use embedded Linux on their own, and organisations who want to create their
own embedded Linux distributions on a common reference.

Embedded Debian would not merely be a different flavour of Debian, but one of
the many flavours of Debian. It would provide a common infrastructure that
minimises the "feature friction" between the vendor distributions based on it.
It would be a more vibrant/dynamic base distribution than any other embedded
Linux.

In contrast to OpenEmbedded which aims to provide tools to make distributions
like Familiar run on embedded devices using an incompatible derivative package
format, Emdebian aims to provide tools to make Debian run on embedded devices
using the normal Debian package format. The Familiar distribution contains
either Opie or GPE and is specifically for certain embedded devices - Emdebian
is working towards GPE inclusion in Debian to allow scaling to any device, from
the smallest embedded device to a full desktop on low resource hardware.

This paper details the updates within Emdebian since DebConf6.

\section{Emdebian}

Running Debian on embedded devices covers a wide range of possible scenarios.
The "embedded" hardware can be anything from a full-blown PC to a MMU-less
thing with a few MB of RAM and flash.  There is no one solution that suits
all of these scenarios at once, so Embedded Debian provides infrastructure and
tools to produce systems according to particular project needs rather than a
fixed package list.

The aim of the project is to provide:

\begin{itemize}
  \item{minimised Debian packages, with the same sort of consistency that Debian itself offers but a much smaller footprint}
  \item{a complete Debian-based environment in which to build and customise them}
  \item{and finally, complete distributions for various devices or types of devices}
\end{itemize}

The composite method has been developed to combine previous threads and create
a cross-build environment based on stag for minimised Debian packages using a
new package: emdebian-tools, currently in Debian unstable and testing. This
results in cross-building a base distribution called Emdebian, suitable for
small systems including built-in package management and the Slind
toolchain-building methods and installer.

\subsection{Features of Emdebian}

\begin{itemize}
 \item{Official sub-project of Debian. Many - but not all - Emdebian developers are also Debian developers}
 \item{Brings Debian to embedded systems, opening up a wider range of applications and environments to support a wider range of embedded devices}
 \item{Takes full advantage of the existing multi-architecture support by using
 build logs from native builds to provide cache data when configuring cross-builds
 for the same arch.}
 \item{Provide free, community supported, embedded development tools}
 \item{Fills the gap between iPAQ and desktop by enabling support for the majority
    of Debian packages. More powerful devices can use a larger and larger
    selection of packages whilst still running Emdebian. High-end embedded
    devices have the option of running Emdebian or normal Debian without
    re-installation}
\end{itemize}

\subsection{The main benefits of Debian for Emdebian.}
\begin{itemize}
 \item{The main non-commercial GNU/Linux distribution}
 \item{Respected package management}
 \item{Supports multiple architectures}
 \item{Huge number of packages (18,000+)}
 \item{Strict quality assurance}
\end{itemize}

Subsequent chapters aim to demonstrate how these Debian features are put to use
within Emdebian.

\section{History}

\subsection{History of Emdebian}

\begin{itemize}
 \item{Project started in 2000 by Frank Smith}
 \item{Produced Embedsys and a toolchain}
 \item{In 2001 update to CML2 and new project lead}
 \item{In 2003 Embedsys updated to Kconfig}
 \item{In 2006 new start, new approach with the Slind and the composite method}
\end{itemize}

The result is a composite of many of these attempts. Readers who remember Stag
should note that it has now been folded into the composite method alongside
Slind as all three methods used dpkg-cross, which is also the basis of
emdebian-tools.

\subsection{Technical challenges}
\begin{itemize}
  \item{Debian is mostly about native development - most packages require some manual
    changes to debian/rules. Native is good, but cross-building is still useful.}
  \item{The size of the binaries is not an issue on desktop systems, resulting in a
    packaging model that encourages all packages to build with all possible
    options enabled. Emdebian turns this around and tries to disable as much as
    possible in order to reduce dependencies. For embedded devices, the rule is: 
    {\tt``small is good, tiny is better''}}
  \item{Much of the documentation included in Debian is not needed on an
    embedded system. No matter how good the documentation may appear, there
    simply isn't room on many embedded devices. This includes licence
    information, changelogs, README documents, man pages, info pages and
    examples.}
  \item{Optimisations in the Debian package may have to be changed for the Emdebian
    package, resulting in awkward bugs.}
  \item{Software management issues on the target compared to the host involve
    problems with available temporary storage, changed dependencies, removal of
    "essential" packages (like debconf and perl) and longer processing times
    for devices that are intended to be mobile and always ready. Translations
    are spun-out into dedicated packages per translation, necessitating a new
    langupdate tool to adapt apt to install any new translations that become
    available for the embedded device locale.}
  \item{Using C libraries other than glibc}
\end{itemize}

\section{The Composite Method}

The composite method consists of an abstraction of previous models, retaining
the key features with the aim of reusing as many existing Debian tools as
possible, without patches, whilst retaining flexible support for
cross-building. The composite method is as close as Emdebian currently gets to
a formal Policy statement and is the basis of all programs in the
emdebian-tools package. The net result of the method is that patches are
applied to each Debian source package before each cross-build rather than to
the Debian package management tools. This allows for fine control over
individual package changes but does increase the workload.

The Debian package management system, including reprepro, apt, dpkg, cdbs,
debhelper, dpatch and debconf is also used in Emdebian and the emdebian-tools
scripts are tightly integrated with the Debian systems. This makes the
transition from Debian to Emdebian as smooth as possible but necessarily raises
the entry barrier for non-Debian contributors.

Emdebian is a sub-project of Debian and the composite method relies on the way
that Debian manages packages in order to bring the benefits of a scalable,
flexible and comprehensive cross-building platform and distribution to Debian.
Whilst contributors to Emdebian do not have to be Debian developers, it is
essential that Emdebian contributors have a thorough understanding of Debian
package management before expecting to use emdebian-tools. Patching each source
package means that Emdebian developers need to understand the package
management tools - particularly debhelper and dpkg - as well as being confident
in working with debian/rules and associated files.

The emdebian-tools package is not designed to be transferable to another base
distribution, GNU/Linux or otherwise. (It is a native package in Debian.)

Whilst the emdebian-tools scripts are written in Perl, there is no need for the
cross-built packages to require Perl as an Essential package. This means that
debconf will not be Essential either.

Other tools within Emdebian are used to build, manage and update the binary
toolchains for cross-building on i386, amd64 and powerpc which can be installed
via emdebian-tools or separately. Developers who do not wish to use
emdebian-tools directly can use these toolchains to support whatever
cross-builds they wish to create. For this reason, Emdebian supports toolchains
for gcc-3.3, gcc-3.4, gcc-4.0 and gcc-4.1. gcc-4.2 toolchains will be available
in due course as emdebian-tools only uses the current default version of gcc in
Debian unstable.

emdebian-tools depends on dpkg-cross and apt-cross (both native Debian
packages) as well as a range of other tools such that installing emdebian-tools
and subversion should ensure that all packages necessary for cross-building are
available. subversion is optional (see later).

\subsection{Summary of the composite method}

\begin{itemize}
  \item{All parameters are controlled in normal \texttt{debian/control} files and all
    changes are shown in the \texttt{.diff.gz} file uploaded to the Emdebian repository.}

  \item{Wrappers for existing build scripts (emdebian-tools) ensure that patches
    are applied in the correct sequence, update the apt-caches for the target
    architecture, cross-build the patched package and automate the management
    of emdebian patch files and build logs using subversion.}

  \item{Emdebian patches remove unwanted build information, documentation and
    Debian installer information, and add individual locale handling. This
    splits out the individual translation files from a single Debian package
    into a set of emdebian packages without any translation files and a series
    of emdebian locale packages, one per translation. This allows individual
    users to only install translation files for the specific locales supported
    by the device. Some packages have 70 translations, maybe 30kb each,
    resulting in a significant reduction in installation size requirements at
    the expense of more packages in the repository. Locale packages use a
    generated package name of \${source}-locale-\${lang} and a generated
    description.}

  \item{Packages are managed by a cross-built apt on the target and a new language
    tool: langupdate which reads the list of installed packages, reads the
    cache, and offers the user a list of available translation files for the
    current user locale(s) which are not installed.}

  \item{Normal Debian tools are used for installing and managing the packages on
    the development host. The Emdebian packages can be uploaded to a normal
    Debian repository and can be used with apt and dpkg without problems.}

  \item{The package itself is essentially the same (only smaller).}

  \item{Identify packages and build files using an emdebian version string: emN
    where "N" is a sequential digit. Version 1:2.3-4 in Debian becomes
    1:2.3-4em1 in Emdebian. An Emdebian fix would be released as 1:2.3-4em2 and
    a new Debian release would be 1:2.3-5em1 or 1:2.4-1em1.}
\end{itemize}

\subsection{How each packaging method is handled}

\subsubsection{CDBS}
emdebian-tools installs a replacement makefile for CDBS -
\url{/usr/share/emdebian-tools/emdebhelper.mk} and patches \url{debian/rules}
to use that instead of \url{/usr/share/cdbs/1/rules/debhelper.mk}.  The
emdebian makefile simply omits calls to \texttt{dh\_installman} and other debhelper
routines that would install unwanted elements into architecture-dependent
binaries. \texttt{debian/control} is also patched to remove udeb and doc packages and
CDBS then omits all rules intended for such packages. Overall, CDBS packages
are by far the easiest packages to cross-build for Emdebian.

\subsubsection{debhelper}
emdebian-tools creates a patch to remove all relevant \texttt{dh\_*} rules from
\texttt{debian/rules} in order to omit documentation. \texttt{debian/rules} needs to be
checked, and then edited to then remove manual dpkg or install commands
that are not removed by the patch. \texttt{debian/control} is also patched to remove
udeb and doc packages, but \texttt{debian/rules} may need some manual changes to
prevent errors when these packages are removed. Some debhelper programs do
not include support for a cross-compiler in \texttt{debian/rules} and this needs to
be added manually. Some debhelper packages also specifically call cc
instead of \${CC} in \texttt{debian/rules} and this needs to be modified. All
modifications to \texttt{debian/rules} are managed by emdebian-tools by updating the
patch each time a build is started.

\subsubsection{dpkg and other methods}

emdebian-tools does not patch \texttt{debian/rules} if neither CDBS nor
debhelper are used. \texttt{debian/rules} needs to be edited by hand to remove
all manpages, infopages, changelogs, licences, copyright files, examples,
documentation, \texttt{README}, \texttt{NEWS}, \texttt{TODO} and any other
unwanted files.  \texttt{debian/control} is patched to remove udeb and doc
packages. All modifications to \texttt{debian/rules} are managed by emdebian-tools by
updating the patch each time a build is started.

The composite method retains the ability to build debian and emdebian from the
same source simply by applying or deapplying patches. Keeping compatibility
means keeping the \texttt{.deb} filename so the \texttt{.mdeb} idea was dropped.
Dependency changes may complicate installation on the development machine, but a mixed
system is possible.

\section{Patch sequencing}

\texttt{debian/rules} cannot patch itself, nor can \texttt{debian/patches} be used to patch
\texttt{debian/control} or \texttt{debian/changelog} so emdebian patch files are located and
maintained in the directory above the \texttt{\${top\_srcdir}} of the package - where the
\texttt{.deb} and \texttt{.diff.gz} files are created. The patches are then applied by
emdebian-tools when a Debian package is "emdebianised", before
dpkg-buildpackage or \texttt{debian/rules} are executed.

\subsection{emdebian patch files}

\begin{itemize}
  \item{emdebian-changelog.patch - implements the emdebian version change by
    calling debchange}

  \item{emdebian-control.in.patch - may be absent if the package does not use
    debian/control.in}

  \item{emdebian-control.patch - removes documentation and installer packages and
    maintains any other changes to the dependencies of the packages}

  \item{emdebian-rules.patch - maintains all changes to the rules for
    cross-building the package}
\end{itemize}

There is also a Wiki page of useful snippets to add or modify debian/rules.
\url{http://wiki.debian.org/EmdebianGuide}

\subsection{Using emdebian-tools}

A Wiki page ( \url{http://wiki.debian.org/EmdebianQuickStart}) is also available for
this section.

% TODO: something sensible
\begin{verbatim}
  + sudo apt-get install emdebian-tools subversion

        This will ask you where you want to build packages, your username for
        emdebian svn access if you have one and whether you want to use apt-get
        or aptitude for toolchain package installation. dpkg-cross will offer
        you the chance to set a default cross-building architecture which saves
        a lot of typing later on. If you choose None, you will need to add
        '--arch ARCH' to all the emdebian-tools commands.

  + Run emsetup --simulate to see what needs to be modified.

    New users are recommended to use at least one -v|--verbose option to all
    emdebian-tools commands.

    $ emsetup --verbose --simulate

    Check that the output is acceptable, nothing has been installed yet.
    Changes during installation can be reversed by purging emdebian-tools.

  + Re-run emsetup. This sets up the ~/.dpkg-cross directory which contains apt
    caches and configuration files. emsetup then installs the Emdebian
    toolchain. apt will keep your toolchain updated as normal.

    $ emsetup -a m68k

    Once emsetup is complete, you can use apt-get update and apt-get upgrade to
    ensure you have the latest versions of apt-cross and emdebian-tools because
    updates are uploaded to Emdebian between Debian versions.

  + Obtain the Debian package, with emsource (which also runs em_make) to
    create the SVN records and emdebian patch files.

  + Change into the new directory, check debian/rules and execute emdebuild. If
    the build is successful, emdebuild --svn adds the .build log to SVN.

  + Upload the generated .changes file to the Emdebian target repository.
    (emdebuild supports a --sign option which is recommended when uploading to
    Emdebian.)
\end{verbatim}

\subsection{emsource and Subversion}

The emdebian patch files of Emdebian packages are stored in Emdebian SVN, along
with the build logs created by successful builds with emdebian-tools. To
automate the version control of these patch files, emdebian-tools contains the
optional emsource script that prepares an SVN working copy in a preset
directory beneath the emdebian working directory (in turn set by debconf). The
Debian source package is unpacked beneath that directory, allowing SVN to keep
the patch files under version control. If the developer has Emdebian SVN commit
access, a successful build log and the emdebian patch files that were used to
create the build can be committed to Emdebian SVN to assist other developers or
to provide a platform for building the next Debian release of the package.

emsource is an optional component of emdebian-tools intended to support
Emdebian SVN. When building packages that are not destined for the Emdebian
repository, there is no need to use emsource, instead the usual Debian methods
of apt-get source or dget are likely to be more useful. \texttt{em\_make}
needs to be run in the directory created by unpacking the source with apt or
dpkg before the emdebian package can be built. emsource will run
\texttt{em\_make} for you.

emdebuild can then be used to build the package. If the build is successful,
\texttt{emdebuild --svn} can be used to commit modified patches and a build log to
Emdebian SVN.

\subsection{Example session with emsource}

\texttt{emsource<package>} downloads a debian source package, unpacks it and
applies or generates the emdebian patch files. This is directly equivalent to
apt-get source in mainstream debian and then applying some patches. emsource
tells you where the package has been unpacked - in an SVN tree beneath your
emdebian working directory. (target denotes that these packages are intended
for installation directly onto the target embedded device.)

\begin{verbatim}
$ emsource -v foo
Using foo 0.1.23.4-0.1
Working directory: '/path/to/working/dir'
Checking for existing emdebian patches
Checking out working copies of existing emdebian patches
Checked out revision 1234.
Checking for existing build tree in foo-0.1.23.4.

Emdebianised source tree for 'foo' exists at
'/path/to/working/dir/target/trunk/f/foo/trunk/foo-0.1.23.4'
Change to this directory before running 'emdebuild'
\end{verbatim}
Now you can cd into the package directory and run emdebuild to cross-build the
package. As with any automated packaging process, take a moment to read
through the amended \texttt{debian/rules} before attempting to build. You will
need to install any build dependencies as needed with apt-get or aptitude.
emdebuild creates a .build log for you. e.g. for package foo:

\begin{verbatim}
$ cd /path/to/working/dir/target/trunk/f/foo/trunk/foo-0.1.23.4
$ emdebuild -v
\end{verbatim}
Packages, .changes, .dsc, .diff.gz, patch files and the .build log are created
in the directory above:

\begin{verbatim}
$ ls /path/to/working/dir/target/trunk/f/foo/trunk/
\end{verbatim}


\section{Package Management and Workflow}
\begin{figure}[htb]
\begin{center}
\includegraphics[width=7cm]{30-emdebian/usualpackaging.png}
\caption{Usual Debian packaging.  Lines in red are problematic for embedded systems.\label{emdebian-normal-pkg-mgmt}}
\end{center}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=7cm]{30-emdebian/emdebianpackaging.png}
\caption{Emdebian package management.  Blue boxes are replacements, note lack of ability to build package ON the target.\label{emdebian-pkg-mgmt}}
\end{center}
\end{figure}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=7cm]{30-emdebian/workflow.png}
\caption{Emdebian workflow. \label{emdebian-workflow}}
\end{center}
\end{figure}

\section{Future}

\subsection{Current and future development}
% TODO: list
\begin{itemize}
  \item{Automate emdebian toolchain updates}
  \item{Package GPE for ARM}
  \item{Cross-compiling with 'emdebuild'}
  \item{Improve documentation of cache files used with './configure –cache-file'}
  \item{Complete 'empdebuild' to support chroot development akin to pdebuild.}
\end{itemize}

\subsection{Getting help or helping out}
% TODO: list
\begin{itemize}
  \item{Subscribe to the mailing list (debian-embedded@lists.debian.org) \url{http://lists.debian.org/debian-embedded/}}
  \item{IRC channel on freenode (\#emdebian)}
  \item{\url{http://www.emdebian.org}}
  \item{Convince package maintainers to support cross-compilation}
  \item{Test it on different platforms and submit problem descriptions (if any)}
\end{itemize}


\section{Acknowledgements}
\begin{itemize}
  \item{\url{www.aleph1.co.uk}}
  \item{Many individuals}
    \begin{itemize}
     \item{Wookey}
     \item{Phillipe de Swert}
     \item{Neil Williams}
     \item{Peter Naulls}
     \item{Jon Masters}
     \item{Justin Cormack}
     \item{Nikita Y. Youshchenko}
     \item{Raphael Bossek}
     \item{Allen Curtis}
     \item{Benjamin Henrion}
    \end{itemize}
  \item{and anyone I might have forgotten}
\end{itemize}

