# Debtags paper for Debconf 7

<!--
	 Copyright (C) 2007  Enrico Zini <enrico@enricozini.org>
   License: GNU GPL v2 or later.
-->

Paper contents:

 * [Introduction](intro)
 * [Available data sources](sources)
 * [Procedures](procedures)
 * [Data formats](formats)
 * [Algorithms](algo)
 * [Development](devel)
 * [Resources](resources)
 * [Links](links)

<!--
# Discussion
#  The community tag contribution / review process
-->

[[tag tags/tips tags/debtags tags/pdo tags/eng]]

<!--
  vim:set ts=2 sw=2:
-->
