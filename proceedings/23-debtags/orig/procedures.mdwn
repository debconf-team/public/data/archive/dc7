# Debtags procedures

## Tagging packages

### Tools

Everyone can tag new packages using the web interface, `debtags` or
`debtags-edit`.

The main point of access for the web interface is the ["Go tagging!"
page](http://debtags.alioth.debian.org/todo.html), but it is possible to access
the [tag editor](http://debtags.alioth.debian.org/edit.html) for one package
directly using an URL like
`http://debtags.alioth.debian.org/edit.html?pkg=`*pkgname*

Tagging can also be done using `debtags-edit`, a GUI application that is not as
good as the web interface, but allows to work offline.  `debtags-edit` has a
`File/Mail changes to central database` option to send your changes to the
database on Alioth.

Tagging can also be scripted using `debtags`: run `debtags help tag` for quick
help on adding, removing and listing tags.  You can submit your local tag
changes using `debtags submit`.

### Tips

There are various tips for what tags to use for your packages:

 * Every package should have one or more tags from `role::`.
 * Every package containing software should have one or more tags from
   `interface::`.
 * Each package containing software or libraries should have one or more tags
   from `implemented-in::`.

The web interface will give you more.  In fact, there is a fascinating program
that scans the tag data for tag relationships and automatically generates more
tips.

If you want to do some more general QA work on tags, you can start from the
[suggestions posted by Justin B.  Rye](http://lists.alioth.debian.org/pipermail/debtags-devel/2005-September/000845.html):

> The following tools have been particularly useful for working out what
> unfamiliar packages are all about.
>
>  * apt-cache (obviously; but nb "apt-cache rdepends")
>  * apt-file ("does it put anything in /usr/bin?  In init.d/?")
>  * debman, in debian-goodies ("what does its man page say?")
>  * surfraw (instant lookups of packages.debian.org/foo)


## Tag workflow

This is the path of a tag contribution on its way to becoming a full part of
Debtags:

 1. The contribution arrives in the Alioth database, either using the [web
    interface](http://debtags.alioth.debian.org/todo.html) or via a mail
    submission.  It becomes visible right away on the Debtags pages on Alioth.
 2. I do a periodical manual review of the changes, approving the good ones and
    discarding what is obviously wrong.  This results on a tag database
    maintained on svn at `svn://svn.debian.org/debtags/tagdb/tags`
 3. From time to time I use the tags in svn to generate a "tag override" file
    and I upload it to the attention of the [ftp-masters](http://ftp-master.debian.org/)
 4. The ftp-masters eventually install the override file in the archive and the
    changes become available to `apt-get update`.

The "override" file is a file that is used to amend (add or change fields) the
control files written by the package maintainers.

I have [run an experiment](http://lists.alioth.debian.org/pipermail/debtags-devel/2006-September/001334.html)
of allowing the maintainers to specify tags in the control file, but it has not
proven satisfactory:

 * some tags are more easily added by people who are not DDs: the maintainer
   can add `made-of::*` and `interface::*`, but may not be the best person to add
   tags such as `works-with::*` or `accessibility::*`.
 * occasionally we do a reorganization of the tag database (for example, we
   could rename `protocol::jabber` to `protocol::im:jabber`) and it is not
   practical to ask all maintainers involved to reflect those changes in their
   control files.
 * the [web interface](http://debtags.alioth.debian.org/edit.html) is by far
   the best way to enter tags anyway.

## Creating new tags

If you see the need for a new tag, post a message to [the mailing
list](http://lists.alioth.debian.org/mailman/listinfo/debtags-devel)
([here's an example](http://lists.alioth.debian.org/pipermail/debtags-devel/2006-December/001500.html)).

This is a short checklist you can use before submitting a new tag:

 * It should represent a clear, atomic concept
 * It should have a facet to fit in
 * Find 6 or 7 packages in Debian that can make use of it

Don't forget to post the list of 6 or 7 packages together with the tag request,
so that after the tag is added it will also be associated to some packages.

Remember that categorisation in Debtags happens with a combination of tags;
this means that instead of having a "`dvdplayer`" tag, we have the combination
"`use::playing, works-with::video, hardware::storage:dvd`".

These combinations also allow to create reasonable approximations of tags that
should not be added because they are not yet used by many packages. For
example, the tag `devel::lang:brainfuck` should not yet be added because the
corresponding packages in Debian are too few, but it can be reasonably
approximated using combinations of `devel::interpreter`, `devel::compiler` and
`use::entertaining`.


## Reorganising tags

There is sometimes the need of doing some reorganisation of the tag vocabulary,
such as renaming a tag, splitting a tag in two or more different tags or
merging two tags into one.

In those cases, the changes are announced and discussed in the [mailing
list](http://lists.alioth.debian.org/mailman/listinfo/debtags-devel) before
proceeding, and when the discussion reaches a conclusion I do my best to
implement the reorganisation without things breaking.

Implementing a reorganisation usually follows a procedure like this:

 1. Add the new tags to the vocabulary.
 2. Add a note to the short description of the tags that will be deleted
    marking them obsolete.
 3. Install the vocabulary in the Alioth database.
 4. Submit the new tags.
 5. Remove the obsolete tags.
 6. Remove the obsolete tags from the vocabulary.

This procedure makes it so that it can be interrupted at any time without loss
of data, and people unaware of it can contribute tags without their
contributions being lost.


## Automated mass-tagging

Sometimes it can be practical to do periodical mass-tagging of packages.  For
example, one can easily infer library packages from the package names, and add
the library-related tags as appropriate.

For this task there is a script called `autodebtag` in the subversion
repository at `svn://svn.debian.org/debtags/autodebtag/trunk`, which allows to
easily plug in new snippets of code.

autodebtag produces a tag patch that can be submitted to the alioth database
using `debtags submit` after inspection.

autodebtag is however not scheduled to run automatically because I lack the
time for reviewing the big patches to make sure there are no flaws in the
scripts.
