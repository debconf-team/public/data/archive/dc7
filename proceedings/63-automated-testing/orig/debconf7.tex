\documentclass[11pt,twocolumn]{article}

\pdfinfo
{ /Title (Automated Testing of Debian Packages: Status Update)
  /Creator (Lucas Nussbaum)
}
\usepackage{vmargin}
\setpapersize{A4}
\setmargnohfrb{0.8in}{1.3in}{0.8in}{1.1in}
\usepackage{varioref}
\usepackage{times}
\usepackage{verbatim}
\usepackage{url}
\urlstyle{sf}
\usepackage{graphicx}
\usepackage{comment}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{xspace}
\usepackage{figlatex}
\usepackage[margin=10pt,font=small,labelfont=bf]{caption}

\newcommand{\etch}{\textit{Etch}\xspace}
\newcommand{\lenny}{\textit{Lenny}\xspace}

\begin{document}

\date{}

\title{Automated Testing of Debian Packages: Status Update}
\author{Lucas Nussbaum\\ \small\texttt{lucas@debian.org}}

\maketitle
\thispagestyle{empty}

\begin{abstract}

During the months preceding the etch release, I worked on several large-scale
QA tasks using an computing grid, including rebuilds of all packages in Etch,
piuparts runs on all packages in Etch, etc. This talk will briefly describe
what was done and how, and then will discuss various directions for future
improvements, including changes needed to run such tasks on a regular basis,
and to make the processes more efficient and scalable.

\end{abstract}

\section{Introduction}

With 10316 source packages in Debian \etch (18167 binary packages), it is
getting increasingly difficult to ensure a good overall quality. While most
other Linux distributions have chosen to split their packages into two groups
of \textit{supported} and \textit{unsupported} packages, all packages in Debian
are supported in the same way.

Being able to check that all packages meet a given quality criteria is very
important. But it is of course impossible to check all packages manually: only
automating such tests can make that possible. Some tests don't require a lot of
resources, even when run over 10000+ packages. This is the case of static tests
like \textit{Lintian} and \textit{Linda}. Others, however, are more expensive
to run, making it hard for a non-profit organization such as Debian to run them
on a regular basis. As an example, rebuilding all source packages in Debian on
a modern AMD64 computer takes about 10 days.

During the months preceding the release of Debian \etch, I had the opportunity
to perform several rebuilds and piuparts tests of all packages in \etch on an
high-performance computing grid called Grid5000~\cite{grid5000,grid5000web,debg5k}, making it
possible to rebuild all packages in Debian in about 7.5 hours. This resulted in
a lot of issues reported and fixed in \etch. The goal of this paper is to
discuss some technical and social issues that arose during that work.

This paper will be divided as follows: first, we will give a short overview of
piuparts, and the test consisting of rebuilding packages from source. Next,
technical issues will be discussed, then social issues.  Finally, some goals
for the \lenny release cycle will be discussed, aiming at improving the process.

\section{Overview of QA tasks}

In this section, a short overview of two QA tasks will be given.

\subsection{Rebuilding packages from source}

Debian packages are only built when they are first uploaded. Also, at that
time, they are not built on the developer's architecture (if a developer is
using an AMD64 system, the package that was built on the developer's system
will go to the AMD64 archive without being rebuilt on the official build
daemons).  Binary packages that apply to all architectures (packages with
\texttt{Architecture: all}) are not rebuilt at all.

One source of problems with that is that developers sometimes build packages in
the environment they use daily, with lots of packages installed, possibly from
unofficial repositories, instead of building them in a clean environment using a
\textit{chroot} or tools like \texttt{pbuilder}. This leads to packages with missing
\texttt{Build-Depends} (especially \texttt{Architecture: all} packages), that
won't build again without modification.

Another source of problems are changes in the build environment. Versions of
packages in \texttt{Build-Depends} are provided manually by the developer, who
usually doesn't investigate exactly which version of the package's dependancy
is required. Also, it's usually not possible to predict the future: the
developer cannot determine that the package will fail to build with a newer
version of a compiler, for example.

The principle of this task is therefore simple: rebuild packages in a specific
build environment, and see if it fails. The environment can be
\textit{testing}, \textit{unstable}, or a custom environment, for example
\textit{unstable} with a newer compiler version installed.

\subsection{Testing package installation with Piuparts}

Piuparts~\cite{piuparts} is a tool used to test installation, upgrade and removal of packages. It does
so by installing packages in an absolutely minimal environment (only containing
\texttt{apt} and all its dependancies).

Its main use is to find problems in \textit{maintainer scripts}.
\textit{Maintainer scripts} are scripts executed during package installation,
removal or upgrade. Package developers often forget that packages using such
scripts must \texttt{Depend} (for \texttt{postinst} scripts, or
\texttt{Pre-depends} for \texttt{preinst} scripts) on the packages needed to
run the script (a package using \texttt{debconf} in its \texttt{postinst} must
\texttt{Depend} on \texttt{debconf}).

Interested readers can refer to \cite{inquis} for more details about piuparts.

\section{Technical issues}

Those tasks raise several technical issues, which will be discussed in this section.

\subsection{Optimizing makespan of rebuilds on a computer grid}

\begin{figure}[b]
\begin{center}
\includegraphics[width=7cm]{sched_rebuilds.fig}
\caption{Example scheduling of package rebuilds. Longer builds are scheduled first, but the total time is limited by the time taken to build openoffice.org.\label{sched-jobs}}
\end{center}
\end{figure}

When building packages on a computer grid, several packages can be built
simultaneously (building one package per compute node). However, using about 40
nodes, and scheduling builds in a smart way (starting with the longest build
first), the time taken to build the whole archive cannot be improved by adding
more nodes: the total time, needed to build all packages, becomes the time
taken by the longest build (see figure~\ref{sched-jobs} for an example).

It is interesting to note that most packages take a very short time to build,
as shown by figure~\ref{fig-buildtimes}: 90\% of the packages need less than
100 seconds, and only 52 packages need more than half an hour. So, if those 52
packages were not built, it would theoretically possible, using enough compute
nodes, to rebuild all of Debian in half an hour !

\begin{figure}[!tb]
\begin{center}
\includegraphics{ecdf-buildtimes-color.pdf}
\caption{Cumulative distribution function of the packages build times. Only a few packages take a long time to build.\label{fig-buildtimes}}
\end{center}
\end{figure}

\begin{table}[!tb]
\begin{center}
\begin{tabular}{|c|c|}
\hline
Source package & Time \\
\hline
openoffice.org & 7 h 14 min \\
latex-cjk-chinese-arphic & 6 h 18 min \\
linux-2.6 & 5 h 43 min \\
gcc-4.1 & 2 h 52 min \\
gcj-4.1 & 2 h 44 min \\
gnat-4.1 & 1 h 52 min \\
gcc-3.4 & 1 h 50 min \\
installation-guide & 1 h 45 min \\
axiom & 1 h 44 m \\
k3d & 1 h 39 min \\
\hline
\end{tabular}
\caption{Packages from \etch that take the longest time to build\label{tab-longbuilds} (on a dual-Opteron 2 GHz)}
\end{center}
\end{table}

It is probably possible to improve the build time of most packages. The easiest
way to do that is to use several CPUs when building. Unfortunately, there is no
standardized way (in policy) to ask for a build using several CPUs. Bug
\#209008 proposes to solve that by using
\texttt{DEB\_BUILD\_OPTIONS="parallel=n"}. This would also benefit all the
developers using dual-core system, which are becoming more common nowadays.

\subsection{False positives during Piuparts tests}

The main issue when doing piuparts tests is false positives (tests that fail
despite a correct package).

Some packages fail to install during piuparts tests because they rely on
another package being installed and configured, but this other package might
require manual configuration. Such failures look impossible to avoid.

Another more important class of false positives is composed of packages which
need to ask questions to the user during their installation. \texttt{debconf}
allows packages to be installed non-interactively, but there are still some
packages prompting manually in the archive (prompting using \texttt{debconf} is
only \textsl{recommended} by the policy). Bug \#206684 discusses making the use
of \texttt{debconf} for prompting mandatory, which would clearly help.

\section{Social issues}

Social issues were also encountered during this work, and as often, they are
much harder to solve than technical issues.

\subsection{Lack of manpower}

\begin{flushright}
<Lunar> I think I'm becoming a perverse...\\I enjoy reporting FTBFS.
\end{flushright}

A common problem in the Free Software community is the lack of manpower,
especially for tasks not really considered fun. While it is "fun" to write
tools to find problems, it is less "fun" to report those problems. This results
in a number of very good tools to find issues in Debian packages, but those
issues are never fixed in the packages, or even reported to the maintainers.

A similar problem exists with rebuilds of the archive. It is "fun" to run a
build daemon, but this can easily be left to people with more computing
resources. A back-of-the-envelope calculation shows that building all packages
in Debian (which takes about 10 days) on a computer consuming 250~W costs about
30 euros.

Reviewing logs of tests and reporting bugs is a tedious task, but it can easily
be shared amongst several developers, making it possible to review more
failures, and even making it more "fun".

\subsection{State of the archive}

The Debian archive contains a lot of packages in a poor state, like packages
with release-critical bugs opened for a long time, without anyone interested in
fixing them.  This makes running large-scale tests difficult, because of the
number of failures to analyze. It might be a good idea to remove some of
packages that have been RC-buggy for a long time, and are not going to be
fixed. But since the decision to remove a package from Debian can only be taken
by the package maintainer, this is usually a long process, especially when
inactive maintainers are involved.

Also, some maintainers aren't well informed of the status of their packages.
After the \etch release, the list of packages in unstable that were not included
in \etch was reviewed, and it was clear that many maintainers did not know that
their package was not part of \etch. For example, some maintainers fixed RC bugs,
but forgot to ask for an "unblock".

\section{Goals for \lenny}

This section will describe a few goals for the \lenny release cycle,
besides those already described before.

\subsection{Improving organization of the collab-qa team}

The collab-qa team is an alioth project aimed at sharing data about QA
activities (using the \texttt{qa} repository for that is problematic, since
non-DD cannot have commit rights on it). Communication is done on the
\texttt{debian-qa} mailing list.

After a few months of activity, the project is slowly getting more active, with
more people contributing. But it is still to be determined whether
enough people will participate to allow this process to continue during the
whole \lenny release cycle.

\subsection{Regular builds of the whole archive}

Ideally, QA tasks should be performed during the whole release cycle, on a
regular basis.  Builds are currently being done every 2 or 3 weeks, but it is
not clear yet whether it will be possible to continue to review the failures.

Also, performing some piuparts runs to catch new failures from time to time
would be a good idea as well.

\subsection{Mail notification of maintainers}

It might be a good idea to develop an opt-out system (so maintainers would be
subscribed by default) to notify maintainers by email. This could be used to
remind them of serious issues (a package not being in \textit{testing}, or a
package with an RC bug). And it could also be used as an easier way to report
issues, such as results from automated tests that weren't analyzed carefully
enough to file an RC bug.

But sending automated mails to maintainers is usually considered a bad idea,
and the system will have to be carefully designed to minimize the annoyance and
maximize the usefulness.

\subsection{Additional tests}

A few other tests could be interesting as well. After building packages, the
resulting binaries could be compared with what is already in the archive. Since
some packages were built several years ago, differences are likely to appear.

Piuparts could also be used more extensively, to report other issues, such as
left-over files, dangling symbolic links, etc.

\section{Conclusion and future work}

Automated Quality Assurance is a good way to ensure that the same quality
level is reached by all packages in Debian. This paper gave an short overview
of QA tasks that were performed during the last months of the \etch release
cycle. It also described in more details the technical and social issues that
arose during the process.

Some ideas and goals for the \lenny release cycle were also given. Implementing
them might allow to noticeably improve the overall quality of Debian.

\section{Acknowledgements}

This work has partly been done within the LIG laboratory, jointly supported by
CNRS, INPG, INRIA, and UJF. Some computer resources were provided by the
Grid'5000 platform (further information at \texttt{http://www.grid5000.fr/}).
We would like to thank the French Ministry of Research, the ACI Grid and ACI
Data Mass incentives, the Grid'5000 staff, as well as all the members of
the Debian community who provided feedback.

\bibliographystyle{unsrt}
\bibliography{debconf7}

\end{document}
