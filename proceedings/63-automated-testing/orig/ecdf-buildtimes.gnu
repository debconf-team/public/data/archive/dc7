set xlabel "package build time (s), logarithmic scale"
set ylabel "F(x)"
set terminal postscript eps enhanced monochrome dashed defaultplex "Helvetica" 14
set size 0.6,0.6
set logscale x
set output 'ecdf-buildtimes.eps'
plot [:10000] 'ecdf-buildtimes.data' with line title ''

set terminal postscript eps enhanced color solid defaultplex "Helvetica" 14
set size 0.6,0.6
set logscale x
set grid
set output 'ecdf-buildtimes-color.eps'
plot [:10000] 'ecdf-buildtimes.data' with line title ''

