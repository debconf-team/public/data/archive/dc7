\section{Open Hardware}

First we need to define what open hardware is - people mean several
different things by the term. 

The minimum definition is hardware which is well-enough documented
that you can write drivers for it. There used to be a site (
\url{http://openhardware.com/}) for certifying this. 

Following on from that is hardware that has free firmware, such that
distros such as Debian can provide support for it without you having
to download firmware from elsewhere.

Currently the most popular definition of open hardware is 'Soft
hardware' - i.e. devices written in a hardware description language
(CPUs, buses, controllers, interfaces) then compiled and programmed
into an actual device (usually an FPGA) to make real hardware.
Opencores.org has lots of examples, designs and info.

Finally we come to the definition I am interested in, and am covering in
this article: actual hardware designs which have open licences, so
that they can be copied or derived-from or manufactured freely. There
is a wide range of such designs from trivial things like a
parallel-port reset device through a lot of radio-ham stuff to complex
SBC (Single Board Computer) designs. I will just be concentrating on
the SBC designs here, although the principles apply to any open
hardware design of sufficient complexity.

There are also open mechanical/electrical designs, of which the Reprap
machine is a particularly interesting example, as it is a 3D printer
which in theory can make all its own parts and thus is a
self-replicating open design. I don't believe anyone has actually made
one make itself yet, but they are well on the way. 



\subsection{Open Designs}
There have been quite a few open SBCs over the years -
I will present a few examples to show the sorts of things we are
talking about and the varying licences.
\begin{itemize}
\item LART
\item Simputer 
\item Balloon
\item Flavio Ribeiro's ARM9 Camera
\item Open Graphics Dev board
\item Darell Harmon's SBC
\end{itemize}

\subsection{Open Designs - LART}
\begin{itemize}
\item SA1100, 32MB RAM, KSB add-ons (Figure~\ref{bohl-lart})
\item MIT licence - do what you like, no guarantees.
\end{itemize}

\begin{figure}[htb]
\begin{center}
\includegraphics[height=5cm]{108-balloon/LART.jpg}
\caption{\url{http://www.lart.tudelft.nl/}\label{bohl-lart}}
\end{center}
\end{figure}

This device was probably the first ARM-based open hardware design.
Done by Jan-Derk Bakker and Erik Mouw of TU Delft. It was (just about)
home manufacturable and a few hardy souls made one. This was later
manufactured by Aleph One and Remote 12 Ltd so that people
who couldn't solder that well or didn't want to invest a lot of time
tracking down all the parts could get one. A fine example of how open
hardware licences are supposed to work; the licence in this case being
a dead simple MIT-style one.

This board was the inspiration for the Balloon project.

\subsection{Open Designs - Simputer}
\begin{itemize}
\item StrongARM, Smart card, LCD (Figure ~\ref{bohl-simputer})
\item Free manufacture, copyleft changes, licenced sales.
\item 4000 sold
\end{itemize}

\begin{figure}[htb]
\begin{center}
\includegraphics[height=5cm]{108-balloon/amida_simputer.jpg}
\caption{\url{http://www.simputer.org/}\label{bohl-simputer}}
\end{center}
\end{figure}

This is perhaps the most famous open hardware project to date. It was
intended to provide computing for the Indian masses by having one of
these per village and individuals had smart-cards which stored their
data/config. It was multilingual and provided net access over GSM. The
licence was actually written by a lawyer and intended to be an open
hardware version of the GPL: 'Simputer GPL'. The Simputer trust
decided whether a design counted as a simputer. The main problem was
that if you wanted to sell simputers then you had to buy a licence from
the Trust which cost \$250,000. Only two companies took up this
offer (and made it to production/sales). A few thousand were sold.

\subsection{Open Designs - Balloon2}
\begin{itemize}
\item StrongARM, CPLD, USB, Audio, Expansion bus (Figure ~\ref{bohl-balloon2})
\item Free manufacture and sales, restricted changes
\end{itemize}

\begin{figure}[htb]
\begin{center}
\includegraphics[height=5cm]{108-balloon/balloon.png}
\caption{\url{http://Balloonboard.org/}\label{bohl-balloon2}}
\end{center}
\end{figure}

The Balloon was created by Steve Wiseman in Cambridge, UK who had made
himself a LART and been impressed. The version that made it to
manufacture used the next-generation chip from the the LART - the
StrongARM 1110. It was manufactured and used by a few different
companies in their devices. The licence allowed manufacture, but for
changes you had to ask Steve. 

\subsection{Open Designs - Flavio Ribeiro's ARM9 Camera}
\begin{itemize}
\item ARM9, Altera FPGA, camera chip
\item Distribution, acknowledgement, no changes.
\end{itemize}

%\begin{figure}[htb]
%\begin{center}
%\includegraphics[width=8cm]{balloon/flaviotop.jpg}
%\caption{\url{http://www.ime.usp.br/~fr/sbc}\label{bohl-flavio}}
%\end{center}
%\end{figure}

This design was produced as a dissertation student project. Anyone
can make one but no changes to the design are permitted.

\subsection{Open Designs - Open Graphics Dev board}
\begin{itemize}
\item Big FPGA between PCI and video (Figure ~\ref{bohl-ogp})
\item GPLed schematics and Gerbers: Open PCB.
\end{itemize}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=7cm]{108-balloon/OGP.jpg}
\caption{\url{http://opengraphics.org}\label{bohl-ogp}}
\end{center}
\end{figure}

Part of the project to make truly open graphics cards available, this
is a dev board for developing a graphics card. The board design is GPLed.

\subsection{Open Designs - Darell Harmon's SBC}
\begin{itemize}
\item Atmel ARM9 CPU+ Xilinx FPGA
\item GPL
\end{itemize}

\begin{figure}[htb]
\begin{center}
\includegraphics[height=5cm]{108-balloon/HarmonSBC.jpg}
\caption{\url{http://dlharmon.com}\label{bohl-harmon}}
\end{center}
\end{figure}

An SBC design very similar to the Camera board, but using the GPL.
This guy has done several open hardware designs. 

% UP TO HERE
\section{The Balloon Project}

The Balloon project grew from Steve Wiseman's original Balloon and
Balloon2 designs to become a sizeable collaboration, largely based around
Cambridge people and companies.

Toby Churchill got involved early on, helping get the design to
manufacture, and have used thousands over the last few years in their
Lightwriter products for people who cannot speak. The involvement of
companies in open hardware development is generally necessary because
the costs involved are usually beyond the means of individuals. I
would like to thank all the above groups which have made significant
contributions to the project so far. Aleph One and Guralp have also
built Balloon2s, and in fact it has shipped nearly as many as the
Simputer overall, despite being a lot less famous.

The project has produced the Balloon3 over the last couple of years,
driven by Cambridge University as well as TCL, and a start-up has been
formed to manage production (for which I am now working part-time).
It will be interesting to see whether a viable company can be based
solely on open hardware or not.

Here is the basic spec. The design can be made in various flavours,
for example a low-power CPLD version or a higher-power, more flexible
FPGA version. It can be made single sided for low-profile use or with
all the expansion connectors if you need the extendibility. 

\begin{itemize}
	\item PXA 270 - 520Mhz
	\item 784MB RAM, 1GB NAND, Max 32MB NOR
	\item FPGA - (1 million gates) or CPLD
	\item USB host, slave, OTG
	\item CF, MMC interface, ADC, GPIO
	\item Expansion bus, 16-bit `Samosa' bus
	\item LCD, I2C, Audio, Camera, Touch screen

    \begin{figure}[htb]
    \begin{center}
    \includegraphics[height=5cm]{108-balloon/balloon-fpga.jpg}
    \caption{\url{http://www.lart.tudelft.nl/}\label{bohl-balloon-fpga}}
    \end{center}
    \end{figure}
\end{itemize}

The intent is for it to be used as a computing component, with some
add-on hardware for whatever you specifically need - power management,
GPS, camera, motor-drivers for robotics, etc. A number of boards have
been made by Cambridge University Engineering Dept for their robotics
and computing teaching courses.

It is of course supplied with entirely free software to complement the
open hardware - bootloader, VHDL for CPLD/FPGA, linux kernel+Debian.

\section{Licencing}

A number of different licences have been used for open hardware
designs, as as been mentioned above.

\begin{itemize}
	\item MIT
	\item Simputer - Lawyer-written, Trademark, Licence fee
	\item VHDL licences - Opencores, GPL
	\item Several GPL-based
		\begin{itemize}
		\item Hardware Design Public Licence
		\item Freedom CPU Licence
		\end{itemize}
	\item TAPR Open Hardware Licence
	\item BOHL
\end{itemize}

The people doing 'soft hardware' have tended to use normal software
licences as they are really just doing software. People making actual
hardware have used both existing software licences and written (or
tried to write) specific open hardware licences. There was a lot of
activity around this subject around 2001, when the Simputer Licence
was written and the unfinished HDPL and Freedom CPU Licenses were
created. Then it went quiet again until recently when two new
licences appeared - the TAPR Open Hardware Licence and the Balloon
Open Hardware Licence.

There are good reasons why simply using a software licence for
hardware is not ideal. Hardware is not the same as software in a
couple of important ways, and these affect licence terms.

\begin{itemize}
	\item Copying cost is large, often very large
    	\begin{itemize}
    		\item 4 prototype balloons - EUR5000
    		\item balloon3 derived from balloon2 - EUR125000
        \end{itemize}
	\item Legal requirements
    	\begin{itemize}
    		\item Sale of goods act.
    		\item EMC
    		\item Component use restrictions - Medical equipment
    		\item Batteries - safety, disposal
    	\end{itemize}
	\item Very expensive typos - duff boards
\end{itemize}

The fundamental difference is that the copying cost is \underline{much} higher. I
cannot just download a board over the internet if I want to hack on it
- it actually has to be made, tested and shipped. The costs of this
are quite high: even for the balloonboard project where most of the
designer time is donated, it has still cost quite a lot of money to
get the design to the stage where it is manufacturable (3 revisions).

The other big issue is the legal environment. As soon as you are
selling real hardware a whole load of laws apply, such as the sale of
goods act, stating that things must be fit for purpose, the EMC
regulations, and other stuff about battery disposal, end-of-life
hardware return (in the EU) and so on. 

It is also true that a typo can be a very expensive mistake. If
someone manufactures a load of boards with some mistake in the files
then they will be annoyed at the tens of thousands, or hundreds of
thousands of pounds/dollars/euros they just wasted.

Nevertheless openness is important in hardware just as it is in
software, so that people can develop better systems from existing
ones, can understand their stuff, can fix associated software, can
design additions, and can learn from existing designs.

So we have tried to write a licence which follows copyleft principles
but which is practical to use in a hardware and manufacturing
environment. It distinguishes between designers, manufacturers,
distributors and users because they all have different legal and moral
responsibilities.

Let's look at its requirements in a bit more detail.

\section{Balloon Open Hardware Licence}

\begin{itemize}
	\item Open Source (copyleft) principles
	\item Distinguishes between designers, manufacturers, distributors and everyone
	\item Protect each group from litigation/liability
	\item Design group controls design
	\item Practical commercially 
	\item Allows connection to proprietary hardware
	\item Has Logo to indicate licence terms
	\item Fully documented designs
	\item Firmware outside licence scope
\end{itemize}

Above are the main principles and aspects of the licence. It is a
hardware licence, so it doesn't cover software at all. Firmware comes
under its own licence. Attempting to include firmware in the scope
caused problems, and we decided a clean scope was better, even though
it meant that someone could put non-free firmware on if they wanted. 

The most unusual aspect of the licence is probably the concept of the
design group. 

\begin{itemize}
	\item Set of people responsible for design
	\item Structure and management is up to each group
	\item Releases matter
	\item Defined group needed by manufacturers and distributors
\end{itemize}

The idea here is that for a complex design legal responsibility and
defined releases are very important. You need to \underline{know} which version
you are building and thus, that it will actually work (if you build a
tested version properly). Thus the normal
ad-hoc group of experts who understand a design is formalised a little
so a manufacturer knows who to ask and check things with.

In order to discuss the licence we need to define some terms, and
distinguish between the different aspects of design information.

\begin{itemize}
	\item Hardware Design Files (HDF) - The design itself
	\item Manufacturing Info (MI) - Gerbers, BOM
	\item Design Documentation (DD) - Schematics, pinouts, memory map, user guide
\end{itemize}

So we have the design files (which in practice are often in
proprietary formats because the best design tools are still
proprietary) which circulate in the design group, the manufacturing
information needed to actually make the devices, and all the
information describing the design which is needed by a user.

Below are the terms applying to each of the groups the licence
distinguishes between. Much of this is to ensure traceability of
versions whilst otherwise allowing people to do what they like, and
making clear where legal responsibility falls.  

\begin{itemize}
    \item{BOHL - Everyone}
        \begin{itemize}
            \item May:
                \begin{itemize}
                    \item Redistribute DD
                    \item Manufacture 
                    \item Make minor MI changes
                    \item Make Component changes
                    \item Install firmware
                \end{itemize}
            \item Must:
                \begin{itemize}
                    \item Pass on changes
                    \item Be responsible for own use
                \end{itemize}
            \item Must not:
                \begin{itemize}
                    \item Include in non-BOHL designs
                    \item Misrepresent or remove copyright notices/design mark
                    \item Apply for patents
                \end{itemize}
           \end{itemize}

    \item{BOHL - Manufacturers}
        \begin{itemize}
            \item May:
                \begin{itemize}
                    \item Manufacture freely
                    \item Add identifying marks
                    \item Provide warranty 
                \end{itemize}
            \item Must:
                \begin{itemize}
                \item Comply with legislation, and show compliance on request
                \item Provide MI and DD to users (nominal fee permitted)
                \end{itemize}
            \item Obligations remain, but rights lapse, if licence broken.
        \end{itemize}

  \item{BOHL - Distributors}
        \begin{itemize}
            \item May:
                \begin{itemize}
                    \item Market and distribute freely
                      \item Add identifying marks
                    \item Provide Warranty
                \end{itemize}
            \item Must:
                \begin{itemize}
                    \item Comply with legislation
                    \item Inform users of Licence
                    \item Inform users of any use restrictions
                \end{itemize}
            \item Obligations remain, but rights lapse, if licence broken.
        \end{itemize}

  \item{Designers}
        \begin{itemize}
            \item May:
                \begin{itemize}
                    \item Access and modify HDF freely
                    \item Base a new (BOHL) design on Design Files
                    \item Perform simulations
                    \item Leave design group
                \end{itemize}
            \item Must:
                \begin{itemize}
                    \item Record HDF modifications (in DD)
                    \item Include visible design mark
                    \item Include release ID
                    \item Pass on HDF modifications
                    \item Ensure additions do not infringe 3rd parties
                    \item Provide info on use/config (normally in DD)
                \end{itemize}
            \item Must not:
                \begin{itemize}
                    \item Claim rights over design
                    \item Pass on HDF outside design group
                \end{itemize}
        \end{itemize}
\end{itemize}

So, there is quite a lot of process mixed into the more fundamental
principles there. I would very much like anyone interested in this
subject to help review the licence and see if it can be simplified
whilst still providing good legal 'insulation'. I think there is
currently rather too much process encoded in the licence, but I also
think the principles are sound.

It would be good if this licence was sufficiently well-written to be
useful to a wide range of hardware projects, and to be acceptable to
companies with the resources to make open hardware designs happen. For
that to be true requires input from quite a number of people. The URL
of the full text is \url{http://balloonboard.org/balloonwiki/OpenHardwareLicense}.

Please comment on the wiki, or on the mailing list.
